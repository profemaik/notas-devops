# Mi chuletario para `docker-compose`

## Ejecutar un `docker-compose.yml`

```bash
docker-compose up -d
```

Donde:

* `-d` ejecuta en modo detached (segundo plano).

## Detener un `docker-compose.yml`

_`docker-compose down` detiene los contenedores y elimina la red o redes. Sin embargo, no elimina los volúmenes._

```bash
docker-compose down

# para eliminar los volúmenes creados
docker-compose down --volumes
```
