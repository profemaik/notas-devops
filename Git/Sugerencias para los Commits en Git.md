# Mis Sugerencias para Escribir los Mensajes en Commits Git

Tal como expresa el título, en este documento doy a conocer algunas sugerencias a la hora de escribir los mensajes en los commits de Git. No pretende imponer normas ni criterios rígidos ni mucho menos ser un documento definitivo.

## ¿Qué es un `commit` en Git?

Un `commit`, en Git, es la confirmación de los cambios que se han realizado a archivos o carpetas en una rama particular de un repositorio. Ya que es importante conocer o dejar constancia del por qué o del para qué de los cambios confirmados, el comando `git commit` acepta el parámetro `-m` para especificar el mensaje del commit.

El mensaje en los commits puede ser útil para indicar:

- qué se hizo o qué cambió
- para qué o por qué se realizaron el/los cambios
- cuál era el problema y cómo se resolvió (y porqué se resolvió de esa manera y no de otra)
- que el commit es temporal, está incompleto o es un WIP (Work In Progress) y, por lo tanto no debe ir a producción aún, o hacerle merge con la rama principal del repositorio
- asociar el/los cambios del commit con una incidencia
- entre otros...

*Es importante destacar* que se debería seguir la regla de 1 cambio = 1 commit, es decir, evitar en lo posible introducir muchos cambios en un solo commit.

## ¿Cómo escribir el mensaje de un commit?

No existe una única manera o la mejor manera de hacerlo, es la persona o el grupo de colaboradores del repositorio los que definen sus políticas o criterios de cómo debe ser la estructura de los mensajes en los commits.

Personalmente, concuerdo con lo propuesto por [Helder Burato](https://dev.to/helderburato) a la hora de escribir los mensajes en los commits. Él opina que una línea correctamente formada del asunto/tema de un commit siempre debe poder completar la siguiente oración:
> Si se aplica este commit, "el asunto/tema de tu commit"

Por ejemplo: `Si se aplica este commit, corrige la validación en la actualización de usuarios`

### Partes del mensaje

1. Mensaje simple:

    ```git
    [Asunto o tema a modo de título]

    [Cuerpo del mensaje.]

    [Pie del mensaje.]
    ```

2. Mensaje más elaborado:

    ```git
    [tipo](alcance): [asunto o tema a modo de título - obligatorio]

    [Cuerpo del mensaje - opcional.]

    [Pie del mensaje - opcional.]
    ```

#### Asunto/Tema del mensaje

Contiene una descripción breve pero concisa del cambio. Se debe escribir en imperativo: **"Cambia tal cosa"**, no "Se cambia tal cosa"; **"Refactoriza la clase tal"**, no "Se refactoriza...". El asunto o tema no debe finalizar con punto, "Refactoriza la clase UsuarioController." incorrecto; **"Refactoriza la clase UsuarioController" correcto.**

#### Cuerpo del mensaje

El cuerpo del mensaje es opcional, normalmente se escribe para:

- explicar el motivo del cambio
- explicar, de manera resumida, la solución implementada si ésta es compleja
- contrastar el funcionamiento o comportamiento anterior del sistema con el nuevo comportamiento derivado del cambio aplicado
- hacer saber alguna observación o detalle que se deba conocer

El cuerpo no es un informe monográfico extenso, tampoco es obligado escribirlo: si el tema o asunto del commit es autosuficientemente explicativo no hay ninguna necesidad de escribir el cuerpo del mensaje.

#### Pie del mensaje

Normalmente se usa para colocar, y por lo tanto relacionar, la incidencia o el caso de seguimiento que originó el commit en cuestión; también para añadir cualquier otra metainformación que sea conveniente especificar en el mensaje del commit. Puede ser escrito de varias maneras, una vez más depende de los criterios establecidos, algunas maneras pudieran ser: `Cierra la incidencia #45`, `Soluciona la incidencia #45`, entre otras.

#### Tipo de asunto o tema del mensaje

Algunas personas o grupos de trabajo prefieren prefijar el asunto/tema del mensaje del commit con ciertas palabras claves abreviadas que permiten la rápida clasificación y/o búsqueda de commits por el tipo de cambio que incorpora. Normalmente se prefieren prefijos del idioma inglés ya que tienden a ser cortos y, además, muchos servicios web de control de versiones y desarrollo de software colaborativo basados en Git, como [GitLab](https://about.gitlab.com/) o [GitHub](https://github.com/), aprovechan esta técnica reconociendo dichos prefijos *solamente en inglés* para facilitar ciertas tareas, un ejemplo es el prefijo o etiqueta (o tag) `WIP`.

A pesar de lo anterior, los prefijos o los tags pueden también ser escritos en perfecto español abreviado. No existe una lista definitiva para los tipos (prefijos, etiquetas o tags) de commits (a excepción del prefijo `WIP` que pareciera haberse estandarizado), éstos son definidos por la persona o el grupo de trabajo del repositorio, . Sin embargo, los prefijos más comunes que se suelen encontrar son:

- `chore` - Cambios regulares de mantenimiento
- `docs` - Cambios que solamente afectan la documentación
- `feat` - Implementa una nueva funcionalidad
- `fix` - Corrige algún bug o error
- `refactor` - Refactorización de código (no afecta ninguna funcionalidad del sistema)
- `revert` - Revierte cambios realizados anteriormente
- `style` - Espacios en blanco, estilo de escritura del código (no es igual a refactorizar)
- `test` - Agrega código relacionado con las pruebas (unit tests...)
- `wip` - Del inglés Work In Progress, indica que el cambio o cambios es un trabajo en progreso y, por lo tanto, puede estar incompleto o ser susceptible a fallos

Los prefijos suelen escribirse en minúsculas, sin embargo, también pueden ser escritos en mayúsculas.

#### El alcance del asunto o tema del mensaje

El alcance sirve para añadir información contextual extra al tipo del commit, por eso suele usarse acompañado del tipo. Se debe escribir entre paréntesis, por ejemplo: `chore(saucelabs): actualiza sauce-connect a la version 4.6.2`. El tema de este commit dice que: se hizo un cambio de tipo rutinario (etiqueta chore, el tipo de asunto o tema) cuyo alcance abarca o afecta solamente al paquete saucelabs (el alcance) y cuyo cambio es la actualización de sauce-connect a una nueva versión (el asunto o el tema).

### Reglas comúnmente Compartidas

1. Separa siempre el asunto/tema del cuerpo con una sola línea en blanco (con un único salto de línea) (*si no hay cuerpo no es necesario el salto de línea*)
2. Limita el tamaño de la línea del asunto/tema a 50 caracteres (personalmente es muy poco, creo que hasta 70 estaría bien. Tampoco existe un límite formal establecido)
3. Redacta el asunto/tema siempre en modo imperativo; si sigues el estilo de mensaje simple la primera letra va en mayúsculas.
4. Nunca finalices el asunto/tema con un punto
5. Limita el tamaño de las líneas del cuerpo y el pie del mensaje a 72 caracteres
6. Utiliza el cuerpo del mensaje para explicar el qué del problema, el por qué del cambio y el cómo implementaste el cambio

## Consideraciones Finales

Todo lo anterior son recomendaciones, no son normas estrictas que se deban cumplir de manera obligatoria. Es el grupo de trabajo el que definirá sus propias reglas, políticas o criterios a la hora de escribir los mensajes de los commits. Y, por encima de todo, **¡prima el sentido común!**.

## Referencias

- [Patterns for writing better git commit messages](https://dev.to/helderburato/patterns-for-writing-better-git-commit-messages-4ba0)
- [A Note About Git Commit Messages](https://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html)
- [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)
- [A Case For Conventional Commits in Git](https://betterprogramming.pub/a-case-for-conventional-commits-in-git-d70c65245009)

## Autor

Maikel Carballo - 2021.
