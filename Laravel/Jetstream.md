# Laravel Jetstream

## Instalar Jetstream
__Jetstream debería ser instalado solamente para nuevas aplicaciones. Instalar Jetstrean en aplicaciones de Laravel ya existentes puede resultar en comportamientos o problemas inesperados.__

1. ### Instalar Jetstream

   ```bash
   composer require laravel/jetstream
   ```

2. ### Instalar Jetstream con Inertia

   ```bash
   php artisan jetstream:install inertia

   # con funcionalidad para grupos de usuarios
   php artisan jetstream:install inertia --teams

   # con soporte para SSR
   php artisan jetstream:install inertia --ssr
   ```

3. ### Finalizar la instalación

   ```bash
   npm install

   npm run build

   php artisan migrate
   ```
