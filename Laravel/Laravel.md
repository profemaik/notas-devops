# Mi Chuletario Laravel

## Crear Nuevo Proyecto [PHP y Composer]

```bash
composer create-project laravel/laravel mi-app

cd mi-app

php artisan serve
```

### Con el Instalador de Laravel

```bash
# Establecer Laravel como dependencia global de Composer
composer global require laravel/installer

# Crear el proyecto
laravel new mi-app

cd mi-app

php artisan serve
```

### Con el Instalador de Laravel y Git

```bash
# Crear el proyecto e inicializar su repositorio Git local
laravel new mi-app --git

# Crear el proyecto e inicializar su repositorio GitHub
laravel new mi-app --github
laravel new mi-app --github="--public"
laravel new mi-app --github="--public" --organization="laravel"
```

## Crear Nuevo Proyecto [Docker/Sail]

```bash
curl -s https://laravel.build/mi-app | bash

cd mi-app

./vendor/bin/sail up
```

### Seleccionando Servicios

```bash
curl -s "https://laravel.build/mi-app?with=pgsql,redis" | bash

```

Los servicios disponibles, al momento de escribir este chuletario, son: `mysql`, `pgsql`, `mariadb`, `redis`, `memcached`, `meilisearch`, `minio`, `selenium` y `mailhog`.

Para desarrollar con Devcontainer:

```bash
curl -s "https://laravel.build/example-app?with=mysql,redis&devcontainer" | bash
```

## Instalar Dependencias en Proyectos Existentes

```bash
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v $(pwd):/opt \
    -w /opt \
    laravelsail/php82-composer:latest \
    composer install --ignore-platform-reqs
```

Imágenes de Sail disponibles: `laravelsail/php74-composer`, `laravelsail/php80-composer`, `laravelsail/php81-composer`, `laravelsail/php82-composer`.

## Sail

### Definir `sail` como Alias (en Linux/Debian)

```bash
# en .bash_aliases definir el siguiente alias
alias sail='[ -f sail ] && bash sail || bash vendor/bin/sail'
```

### Usar `sail`

```bash
# iniciar sail en modo "detached"
sail up -d

# detener sail eliminando volúmenes
sail down --volumes

# ejecutar comandos de PHP
sail php --version

# ejecutar comandos composer
sail composer require laravel/sanctum

# ejecutar comandos artisan
sail artisan queue:work

# ejecutar comandos Node/NPM
sail node --version
sail npm run prod
```

### Publicar Sail

```bash
sail artisan sail:publish
```

### Reconstruir Contenedores Sail

```bash
sail build --no-cache
```

## Base de Datos

### Crear Modelos

```bash
# Generar el modelo
php artisan make:model Flight

# Generar el modelo definiendo la ruta de guardado
php artisan make:model Models/Core/Flight

# Generar el modelo y su respectivo migrador
php artisan make:model Flight --migration
php artisan make:model Flight --m

# Generar el modelo y su respectiva factoría
php artisan make:model Flight --factory
php artisan make:model Flight -f

# Generar el modelo y su respectivo alimentador
php artisan make:model Flight --seed
php artisan make:model Flight -s

# Generar el modelo y su respectivo controlador
php artisan make:model Flight --controller
php artisan make:model Flight -c

# Generar el modelo con su migrador, factoría, alimentador, y controlador
php artisan make:model Flight -mfsc

# Atajo para generar el modelo con su migrador, factoría, alimentador, y controlador
php artisan make:model Flight --all

# Generar un modelo "pivot"
php artisan make:model Member --pivot
```

### Crear Migraciones

```bash
# Crear la migración para la tabla users
php artisan make:migration create_users_table

# Crear la migración para la tabla users definiendo la ruta
php artisan make:migration --path=database/migrations/folder create_users_table

# Deshacer la última migración
php artisan migrate:rollback

# Deshacer un número limitado de migraciones
php artisan migrate:rollback --step=5

# Recrear por completo la BD
php artisan migrate:refresh --seed

# Eliminar todas las tablas y volverlas a crear
php artisan migrate:fresh
php artisan migrate:fresh --seed # y, además, alimentarlas

# Ejecutar migraciones especificando ruta
php artisan migrate --path=app/foo/migrations

# Ejecutar una migración específica
php artisan migrate --path=app/foo/migrations.php
```

### Crear Seeders

```bash
php artisan make:seeder UserSeeder
```

### Llenar las Tablas/Relaciones

```bash
# Todas las tablas/relaciones
php artisan db:seed

# Especificando la tabla/relación
php artisan db:seed --class=UserSeeder
```

## Controladores

### Controladores Recursos

```bash
php artisan make:controller PhotoController --resource
```

## Eventos

### Generar Eventos Manualmente

```bash
php artisan make:event PodcastProcessed
```

### Generar Escuchadores de Eventos Manualmente

```bash
php artisan make:listener SendPodcastNotification --event=PodcastProcessed
```

## Autor ✒️

Maikel Carballo - 2021.
