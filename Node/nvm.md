# Node Version Manager

## Instalación y Actualización

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.2/install.sh | bash
```

## Verificar Instalación

```bash
command -v nvm
```

## Uso

### Listar Versiones Remotas Disponibles

```bash
nvm ls-remote
```

### Instalar la última versión LTS

```bash
nvm install --lts
```

### Instalar la última versión LTS y reinstalar paquetes

```bash
nvm install 'lts/*' --reinstall-packages-from=current
```
