# Notas DevOps

Comandos y notas relacionadas con:

- Docker y Docker Compose.
- Git
- Laravel
- GNU/Linux.
- Node.js
- Vim
- Vue

## Autor ✒️

[Maikel Carballo](https://twitter.com/_kimael_) 2021-2022.
