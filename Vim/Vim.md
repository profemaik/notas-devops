# Vim Editor de Texto

## Cerrar/Salir Vim

```bash
# Cerrar/Salir sin guardar cambios
:q!
:cq # return non-zero error
ZQ

# Cerrar/Salir y guardar cambios
:wq

# Cerrar/Salir y forzar guardar cambios
:wq!

# Cerrar/Salir y guardar si solamente si hay cambios
:x
ZZ
```
